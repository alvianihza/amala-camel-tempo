package id.co.asyst.amala.camel.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class Response implements Serializable {

    ObjectMapper oMapper = new ObjectMapper();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Identity identity;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Paging paging;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public BaseParameter parameter;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Map<String, Object> result;

    public Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LinkedHashMap<String, Object> get() {
        LinkedHashMap<String, Object> res = new LinkedHashMap<String, Object>();
        res.put("status", status);
        res.put("identity", identity);
        res.put("parameter", parameter);
        if(paging != null){
            res.put("paging", paging);
        }
        if (result != null) {
            res.put("result", result);
        }
        return res;
    }

    public void destroy() {
        this.result = null;
        this.identity = null;
        this.parameter = null;
        this.status = null;
        this.paging = null;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public Map<String, Object> getResult() {
        return result;
    }

    public void setResult(Map<String, Object> result) {
        this.result = result;
    }

    public Identity getIdentity() {
        return identity;
    }

    public void setIdentity(Identity identity) {
        if (identity.getPassword() != null) {
            identity.setPassword(identity.getPassword().toString().replaceAll("[a-zA-Z0-9_]", "*"));
        }
        this.identity = identity;
    }

    public BaseParameter getParameter() {
        return parameter;
    }

    public void setParameter(BaseParameter parameter) {
        this.parameter = parameter;
    }

}
