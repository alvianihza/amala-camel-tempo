package id.co.asyst.amala.camel.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.pool2.BaseObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseParameter extends BaseObject implements Serializable {
    private static final long serialVersionUID = -7079852813633989802L;

    public BaseParameter() {
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected Map<String, String> sort;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected List<String> column;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected Map<String, String> criteria;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected Map<String, Object> data;

    public Map<String, String> getSort() {
        return sort;
    }

    public void setSort(Map<String, String> sort) {
        this.sort = sort;
    }

    public List<String> getColumn() {
        return column;
    }

    public void setColumn(List<String> column) {
        this.column = column;
    }

    public Map<String, String> getCriteria() {
        return criteria;
    }

    public void setCriteria(Map<String, String> criteria) {
        this.criteria = criteria;
    }

    public void addCriteria(String k, String v) {
        if (this.criteria == null) {
            this.criteria = new HashMap<>();
        }
        this.criteria.put(k, v);
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public void addData(String k, Object v) {
        if (this.data == null) {
            this.data = new HashMap<>();
        }
        this.data.put(k, v);
    }

    @Override
    public String toString() {
        return "BaseParameter{" +
                "sort=" + sort +
                ", column=" + column +
                ", criteria=" + criteria +
                ", data=" + data +
                '}';
    }

}
