package id.co.asyst.amala.camel.model;

import org.springframework.util.StringUtils;

public class BaseValidator {

    private String FIELD_FORMAT_NUMERIC = " must be numeric";
    private String FIELD_FORMAT_ALPHABET = "field.format.alphabet";
    private String FIELD_FORMAT_ALPHABET_UPPER = " must be Alphabet Upper";
    private String FIELD_FORMAT_ALPHABET_SPACE = " must be Alphabet + Space";
    private String FIELD_FORMAT_ALPHANUMERIC = " harus berupa huruf & angka";
    private String FIELD_FORMAT_ALPHANUMERIC_UPPER = " must be Alphanumeric Capital";
    private String FIELD_FORMAT_ALPHANUMERIC_SPACE = " must be Alphanumeric Capital";

    public void notNull(Object value, String field) {
        if (value == null) {
            throw new ApplicationException(Status.INVALID(field + " field is required"));
        }
    }

    public void notBlank(String value, String field) {
        if (StringUtils.isEmpty(value)) {
            throw new ApplicationException(Status.INVALID(field + " field is required"));
        }
    }

    protected void notZero(Object value, String field) {
        notNull(value, field);
        if ((value instanceof Integer && value.equals(0)) ||
                (value instanceof Long && value.equals(0L)) ||
                (value instanceof Double && value.equals(0.0)) ||
                (value instanceof Float && value.equals(0.0f))) {
            throw new ApplicationException(Status.INVALID(field + " field is required"));
        }
    }

    protected void isAlphanumericUpper(String value, String field) {
        if (!StringUtils.isEmpty(value) && !value.matches(Constants._REGEX_ALPHANUMERIC_UPPER)) {
            throw new ApplicationException(Status.INVALID(field + FIELD_FORMAT_ALPHANUMERIC_UPPER ));
        }
    }

    protected void isAlphanumeric(String value, String field) {
        if (!StringUtils.isEmpty(value) && !value.matches(Constants._REGEX_ALPHANUMERIC)) {
            throw new ApplicationException(Status.INVALID(field + FIELD_FORMAT_ALPHANUMERIC));
        }
    }

    protected void isAlphanumericSpace(String value, String field) {
        if (!StringUtils.isEmpty(value) && !value.matches(Constants._REGEX_ALPHANUMERIC_SPACE)) {
            throw new ApplicationException(Status.INVALID(field + FIELD_FORMAT_ALPHANUMERIC_SPACE));
        }
    }

    protected void isAlphabet(String value, String field) {
        if (!StringUtils.isEmpty(value) && !value.matches(Constants._REGEX_ALPHABETH)) {
            throw new ApplicationException(Status.INVALID(field + FIELD_FORMAT_ALPHABET));
        }
    }

    protected void isAlphabetUpper(String value, String field) {
        if (!StringUtils.isEmpty(value) && !value.matches(Constants._REGEX_ALPHABET_UPPER)) {
            throw new ApplicationException(Status.INVALID(field + FIELD_FORMAT_ALPHABET_UPPER));
        }
    }

    protected void isAlphabetSpace(String value, String field) {
        if (!StringUtils.isEmpty(value) && !value.matches(Constants._REGEX_ALPHABETH_SPACE)) {
            throw new ApplicationException(Status.INVALID(field + FIELD_FORMAT_ALPHABET_SPACE));
        }
    }

    protected void isNumeric(String value, String field) {
        if (!StringUtils.isEmpty(value) && !value.matches(Constants._REGEX_NUMBERS)) {
            throw new ApplicationException(Status.INVALID(field + FIELD_FORMAT_NUMERIC));
        }
    }

    protected void isMax(String value, int length, String field) {
        if (!StringUtils.isEmpty(value) && value.length() > length) {
            throw new ApplicationException(Status.INVALID(field + " max length is " + length));
        }
    }

    protected void isMin(String value, int length, String field) {
        if (!StringUtils.isEmpty(value) && value.length() < length) {
            throw new ApplicationException(Status.INVALID(field + " min length is " + length));
        }
    }


}
