package id.co.asyst.amala.camel.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Logging implements Serializable {

    private static final long serialVersionUID = -295422703255886286L;
    ObjectMapper oMapper = new ObjectMapper();

    public Request request;

    public Response response;

    public String executiontime;

    public String modulename;

    public String status;

    private LocalDateTime startdate;

    private LocalDateTime enddate;

    private String operation;

    private String token;

    public LocalDateTime getStartdate() {
        return startdate;
    }

    public void setStartdate(LocalDateTime startdate) {
        this.startdate = startdate;
    }

    public LocalDateTime getEnddate() {
        return enddate;
    }

    public void setEnddate(LocalDateTime enddate) {
        this.enddate = enddate;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private Gson gson = new Gson();

    public LinkedHashMap<String, Object> get() {
        LinkedHashMap<String, Object> res = new LinkedHashMap<String, Object>();
        Map<String, Object> requestconvert = oMapper.convertValue(this.request, Map.class);
        Map<String, Object> responseconvert = oMapper.convertValue(this.response, Map.class);
        res.put("modulename", modulename);
        res.put("operation", operation);
        res.put("token", token);
        res.put("request", requestconvert);
        res.put("response", responseconvert);
        res.put("executiontime", executiontime);
        res.put("status", status);
        res.put("startdate", startdate);
        res.put("enddate", enddate);
        return res;
    }

    public void destroy() {
        this.request = null;
        this.response = null;
        this.executiontime = null;
        this.modulename = null;
        this.status = null;
    }
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public void setRequest(HashMap request){
        this.request = oMapper.convertValue(request, Request.class);
    }

    public void setRequest(String request){
        Map<String, Object> requestconverted = gson.fromJson(request, Map.class);
        this.request = oMapper.convertValue(requestconverted, Request.class);
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public void setResponse(HashMap response) {
        this.response = oMapper.convertValue(response, Response.class);
    }

    public void setResponse(String response){
        Map<String, Object> responseconverted = gson.fromJson(response, Map.class);
        this.response = oMapper.convertValue(responseconverted, Response.class);
    }

    public String getExecutiontime() {
        return executiontime;
    }

    public void setExecutiontime(String executiontime) {
        this.executiontime = executiontime;
    }

    public String getModulename() {
        return modulename;
    }

    public void setModulename(String modulename) {
        this.modulename = modulename;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}